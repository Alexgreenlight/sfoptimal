﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace P1_PoiskOptimPuti
{
    public partial class screamer : Form
    {
        bool sp = false;
        SoundPlayer soundon = new SoundPlayer(Properties.Resources.krik);
        SoundPlayer soundloop = new SoundPlayer(Properties.Resources.closeeyes);
        public screamer()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.F4 && e.Alt)
            {
                MessageBox.Show("Ха-ха! Не так быстро!");
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Escape)
            {
                MessageBox.Show("Ха-ха! Не так быстро!");
                e.Handled = true;
            }
            if (e.KeyCode == Keys.L&& e.Control)
            {
                Close();
                e.Handled = true;
            }
        }

        private void screamer_Load(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.uiMode = "none";
            var strTempFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "layerg.avi");
            try
            {
                File.WriteAllBytes(strTempFile, Properties.Resources.layerg);
                axWindowsMediaPlayer1.URL = strTempFile;
            }
            catch
            {
                MessageBox.Show("Всё плохо со Старостиной");
            }
        }

        private void axWindowsMediaPlayer1_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 1 && !sp)
            {
                axWindowsMediaPlayer1.Ctlcontrols.play();
            }
        }

        private void screamer_FormClosed(object sender, FormClosedEventArgs e)
        {
            sp = true;
            soundloop.Stop();
            axWindowsMediaPlayer1.Ctlcontrols.stop();
        }

        private void screamer_Shown(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.play();
            if (axWindowsMediaPlayer1.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
               axWindowsMediaPlayer1.fullScreen = true;
            }
            soundon.Play();
            DateTime end = DateTime.Now + TimeSpan.FromSeconds(5);
            while (end > DateTime.Now)
            {
                Application.DoEvents();
            }
            soundon.Stop();
            soundloop.PlayLooping();
        } 
    }

}
