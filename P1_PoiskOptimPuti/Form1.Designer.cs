﻿namespace P1_PoiskOptimPuti
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Load1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btRabota = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ResBut = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.op1 = new System.Windows.Forms.RadioButton();
            this.op2 = new System.Windows.Forms.RadioButton();
            this.op3 = new System.Windows.Forms.RadioButton();
            this.dich = new System.Windows.Forms.Button();
            this.dlico = new System.Windows.Forms.PictureBox();
            this.shadow = new System.Windows.Forms.PictureBox();
            this.tipZag = new System.Windows.Forms.PictureBox();
            this.MinPic = new System.Windows.Forms.PictureBox();
            this.MaxPic = new System.Windows.Forms.PictureBox();
            this.ClosePic = new System.Windows.Forms.PictureBox();
            this.pointOK = new System.Windows.Forms.PictureBox();
            this.mapOK = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dlico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shadow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipZag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapOK)).BeginInit();
            this.SuspendLayout();
            // 
            // Load1
            // 
            this.Load1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Load1.Location = new System.Drawing.Point(49, 197);
            this.Load1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Load1.Name = "Load1";
            this.Load1.Size = new System.Drawing.Size(169, 62);
            this.Load1.TabIndex = 1;
            this.Load1.Text = "Загрузка карты";
            this.Load1.UseVisualStyleBackColor = true;
            this.Load1.Click += new System.EventHandler(this.Load1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btRabota
            // 
            this.btRabota.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btRabota.Location = new System.Drawing.Point(264, 363);
            this.btRabota.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btRabota.Name = "btRabota";
            this.btRabota.Size = new System.Drawing.Size(217, 50);
            this.btRabota.TabIndex = 2;
            this.btRabota.Text = "Рассчитать";
            this.btRabota.UseVisualStyleBackColor = true;
            this.btRabota.Click += new System.EventHandler(this.BtRabota_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(185, 150);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(379, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Прежде чем начать, загрузите данные";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(516, 197);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 62);
            this.button1.TabIndex = 5;
            this.button1.Text = "Загрузка точек";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(61, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(228, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Поиск оптимального пути";
            this.label3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label3_MouseDown);
            // 
            // ResBut
            // 
            this.ResBut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResBut.Location = new System.Drawing.Point(317, 241);
            this.ResBut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ResBut.Name = "ResBut";
            this.ResBut.Size = new System.Drawing.Size(100, 38);
            this.ResBut.TabIndex = 12;
            this.ResBut.Text = "Сброс";
            this.ResBut.UseVisualStyleBackColor = true;
            this.ResBut.Click += new System.EventHandler(this.ResBut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(45, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(297, 25);
            this.label1.TabIndex = 13;
            this.label1.Text = "Выбор критерия оптимизации";
            // 
            // op1
            // 
            this.op1.AutoSize = true;
            this.op1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.op1.Location = new System.Drawing.Point(16, 89);
            this.op1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.op1.Name = "op1";
            this.op1.Size = new System.Drawing.Size(203, 24);
            this.op1.TabIndex = 14;
            this.op1.TabStop = true;
            this.op1.Text = "Минимальная длина";
            this.op1.UseVisualStyleBackColor = true;
            this.op1.CheckedChanged += new System.EventHandler(this.Op1_CheckedChanged);
            // 
            // op2
            // 
            this.op2.AutoSize = true;
            this.op2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.op2.Location = new System.Drawing.Point(239, 89);
            this.op2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.op2.Name = "op2";
            this.op2.Size = new System.Drawing.Size(281, 24);
            this.op2.TabIndex = 15;
            this.op2.TabStop = true;
            this.op2.Text = "Минимально прижимающийся";
            this.op2.UseVisualStyleBackColor = true;
            this.op2.CheckedChanged += new System.EventHandler(this.Op2_CheckedChanged);
            // 
            // op3
            // 
            this.op3.AutoSize = true;
            this.op3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.op3.Location = new System.Drawing.Point(544, 89);
            this.op3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.op3.Name = "op3";
            this.op3.Size = new System.Drawing.Size(178, 24);
            this.op3.TabIndex = 16;
            this.op3.TabStop = true;
            this.op3.Text = "Минимум изгибов";
            this.op3.UseVisualStyleBackColor = true;
            this.op3.CheckedChanged += new System.EventHandler(this.Op3_CheckedChanged);
            // 
            // dich
            // 
            this.dich.Location = new System.Drawing.Point(16, 375);
            this.dich.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dich.Name = "dich";
            this.dich.Size = new System.Drawing.Size(43, 34);
            this.dich.TabIndex = 18;
            this.dich.Text = "!";
            this.dich.UseVisualStyleBackColor = true;
            this.dich.Click += new System.EventHandler(this.Dich_Click);
            // 
            // dlico
            // 
            this.dlico.Image = global::P1_PoiskOptimPuti.Properties.Resources.toapp;
            this.dlico.Location = new System.Drawing.Point(-3, -4);
            this.dlico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dlico.Name = "dlico";
            this.dlico.Size = new System.Drawing.Size(53, 44);
            this.dlico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dlico.TabIndex = 19;
            this.dlico.TabStop = false;
            this.dlico.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Dlico_MouseDown);
            // 
            // shadow
            // 
            this.shadow.BackColor = System.Drawing.SystemColors.Control;
            this.shadow.Location = new System.Drawing.Point(16, 363);
            this.shadow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shadow.Name = "shadow";
            this.shadow.Size = new System.Drawing.Size(81, 62);
            this.shadow.TabIndex = 17;
            this.shadow.TabStop = false;
            // 
            // tipZag
            // 
            this.tipZag.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tipZag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tipZag.Location = new System.Drawing.Point(1, -1);
            this.tipZag.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tipZag.Name = "tipZag";
            this.tipZag.Size = new System.Drawing.Size(577, 44);
            this.tipZag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.tipZag.TabIndex = 10;
            this.tipZag.TabStop = false;
            this.tipZag.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TipZag_MouseDown);
            // 
            // MinPic
            // 
            this.MinPic.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MinPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MinPic.Image = global::P1_PoiskOptimPuti.Properties.Resources.sv;
            this.MinPic.Location = new System.Drawing.Point(577, -1);
            this.MinPic.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinPic.Name = "MinPic";
            this.MinPic.Size = new System.Drawing.Size(54, 44);
            this.MinPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MinPic.TabIndex = 10;
            this.MinPic.TabStop = false;
            this.MinPic.Click += new System.EventHandler(this.MinPic_Click);
            this.MinPic.MouseEnter += new System.EventHandler(this.MinPic_MouseEnter);
            this.MinPic.MouseLeave += new System.EventHandler(this.MinPic_MouseLeave);
            // 
            // MaxPic
            // 
            this.MaxPic.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MaxPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MaxPic.Image = global::P1_PoiskOptimPuti.Properties.Resources.razv;
            this.MaxPic.Location = new System.Drawing.Point(631, -1);
            this.MaxPic.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaxPic.Name = "MaxPic";
            this.MaxPic.Size = new System.Drawing.Size(54, 44);
            this.MaxPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MaxPic.TabIndex = 9;
            this.MaxPic.TabStop = false;
            this.MaxPic.Click += new System.EventHandler(this.MaxPic_Click);
            this.MaxPic.MouseEnter += new System.EventHandler(this.MaxPic_MouseEnter);
            this.MaxPic.MouseLeave += new System.EventHandler(this.MaxPic_MouseLeave);
            // 
            // ClosePic
            // 
            this.ClosePic.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClosePic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ClosePic.Image = global::P1_PoiskOptimPuti.Properties.Resources.close;
            this.ClosePic.Location = new System.Drawing.Point(684, -1);
            this.ClosePic.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ClosePic.Name = "ClosePic";
            this.ClosePic.Size = new System.Drawing.Size(54, 44);
            this.ClosePic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ClosePic.TabIndex = 9;
            this.ClosePic.TabStop = false;
            this.ClosePic.Click += new System.EventHandler(this.ClosePic_Click);
            this.ClosePic.MouseEnter += new System.EventHandler(this.ClosePic_MouseEnter);
            this.ClosePic.MouseLeave += new System.EventHandler(this.ClosePic_MouseLeave);
            // 
            // pointOK
            // 
            this.pointOK.BackgroundImage = global::P1_PoiskOptimPuti.Properties.Resources.okpic;
            this.pointOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pointOK.Location = new System.Drawing.Point(583, 277);
            this.pointOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pointOK.Name = "pointOK";
            this.pointOK.Size = new System.Drawing.Size(49, 43);
            this.pointOK.TabIndex = 7;
            this.pointOK.TabStop = false;
            this.pointOK.Visible = false;
            // 
            // mapOK
            // 
            this.mapOK.BackgroundImage = global::P1_PoiskOptimPuti.Properties.Resources.okpic;
            this.mapOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mapOK.Location = new System.Drawing.Point(108, 277);
            this.mapOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mapOK.Name = "mapOK";
            this.mapOK.Size = new System.Drawing.Size(49, 43);
            this.mapOK.TabIndex = 6;
            this.mapOK.TabStop = false;
            this.mapOK.Visible = false;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(736, 428);
            this.Controls.Add(this.dlico);
            this.Controls.Add(this.shadow);
            this.Controls.Add(this.dich);
            this.Controls.Add(this.op3);
            this.Controls.Add(this.op2);
            this.Controls.Add(this.op1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ResBut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tipZag);
            this.Controls.Add(this.MinPic);
            this.Controls.Add(this.MaxPic);
            this.Controls.Add(this.ClosePic);
            this.Controls.Add(this.pointOK);
            this.Controls.Add(this.mapOK);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btRabota);
            this.Controls.Add(this.Load1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dlico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shadow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipZag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapOK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Load1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btRabota;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox mapOK;
        private System.Windows.Forms.PictureBox pointOK;
        private System.Windows.Forms.PictureBox ClosePic;
        private System.Windows.Forms.PictureBox MaxPic;
        private System.Windows.Forms.PictureBox MinPic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox tipZag;
        private System.Windows.Forms.Button ResBut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton op1;
        private System.Windows.Forms.RadioButton op2;
        private System.Windows.Forms.RadioButton op3;
        private System.Windows.Forms.PictureBox shadow;
        private System.Windows.Forms.Button dich;
        private System.Windows.Forms.PictureBox dlico;
    }
}

