﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using System.IO;

namespace P1_PoiskOptimPuti
{
    public partial class IntroForm : Form
    {
        public IntroForm()
        {
            InitializeComponent();
        }

        private void IntroForm_Load(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.uiMode = "none";
            var strTempFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "v1.avi");
            try
            {
                File.WriteAllBytes(strTempFile, Properties.Resources.v1);
                axWindowsMediaPlayer1.URL = strTempFile;
                axWindowsMediaPlayer1.Ctlcontrols.play();
            }
            catch
            {
                MessageBox.Show("Всё плохо с заставкой");
            }
        }

        private void AxWindowsMediaPlayer1_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == 1)
            {
                Close();
            }
        }

        private void IntroForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.Show();
            
        }
    }
}
