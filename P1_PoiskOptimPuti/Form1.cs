﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace P1_PoiskOptimPuti
{
    public partial class Form1 : Form
    {
        SoundPlayer sound1 = new SoundPlayer(Properties.Resources.scary);
        int mode=0;
        bool outp = false;
        bool spl = false;
        List<Traces> LTrack = new List<Traces>();
        MapData DMatr;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            IntroForm frm2 = new IntroForm();
            frm2.Show();
            op1.Checked = true;
            ToolTip tooltip1 = new ToolTip {
                InitialDelay = 100,
                ReshowDelay = 100
            };
            tooltip1.SetToolTip(this.ClosePic, "Закрыть");
            tooltip1.SetToolTip(this.MaxPic, "Развернуть");
            tooltip1.SetToolTip(this.MinPic, "Свернуть");
        }

        private void Load1_Click(object sender, EventArgs e)
        {
            int width = 0, height = 0, nachX, nachY, konX, konY;
            MessageBox.Show("Покорнейше надеюсь, что в файле данные лежат должным образом, без всякой дичи, отделенные любезно одними лишь пробелами, да переходами на новую строку.\nВообще" +
               " же ожидаю видеть всё в таком формате:\nШирина Высота\nНижний левый угол элемента Верхний правый угол элемента", "Обрати внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            OpenFileDialog openFileDialog1 = new OpenFileDialog {
                Filter = "Текстовички (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex = 1
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                    {
                        string line;
                        line = sr.ReadLine();
                        string[] split = line.Split(' ');
                        width = Convert.ToInt32(split[0]);
                        height = Convert.ToInt32(split[1]);
                        if ((width != 0) && (height != 0))
                            DMatr = new MapData(width, height);
                        int nx, ny;
                        nx = DMatr.Getx();
                        ny = DMatr.Gety();
                        for (int i = 0; i < nx; i++)
                        {
                            for (int j = 0; j < ny; j++)
                            {
                                DMatr.Set(i, j, 0, -1);
                            }
                        }
                        string[] sp;
                        while ((line = sr.ReadLine()) != null)
                        {
                            sp = line.Split(' ');
                            nachX = Convert.ToInt32(sp[0]);
                            nachY = Convert.ToInt32(sp[1]);
                            konX = Convert.ToInt32(sp[2]);
                            konY = Convert.ToInt32(sp[3]);
                            for (int i = nachX; i < konX; i++)
                            {
                                for (int j = nachY; j < konY; j++)
                                {
                                    DMatr.Set(i, j, 0, -2);
                                }
                            }

                        }
                    }
                    mapOK.Visible = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Чего-то не удалось :( \nOriginal error: " + ex.Message);
                }
            }
        }

        private void BtRabota_Click(object sender, EventArgs e)
        {
            if ((mapOK.Visible == false) || (pointOK.Visible == false))
            {
                MessageBox.Show("Не соблюдены условия для запуска расчёта!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Bitmap image1;
            int x, y, min = 0,mink=0;
            x = DMatr.Getx();
            y = DMatr.Gety();
            image1 = new Bitmap(x, y);
            Graphics imgr = Graphics.FromImage(image1);
            imgr.Clear(Color.White);
            bool added = true, result = true;

            for (int p = 0; p < LTrack.Count; p++)
            {
                int x_at, y_at, x_to, y_to;
                result = true;
                x_at = LTrack[p].x;
                y_at = LTrack[p].y;
                x_to = LTrack[p].x_to;
                y_to = LTrack[p].y_to;
                //code here
                if ((DMatr.Get(x_at, y_at, 0) != -1) || (DMatr.Get(x_to, y_to, 0) != -1))
                {
                    MessageBox.Show("Расчёт принципиально невозможен");
                    break;
                }
                DMatr.Set(x_at, y_at, 0, 0);// До cтарта ноль шагов - от него будем разбегаться
                min = 0;
                mink = 0;
                // Изначально мы сделали ноль шагов
                // Пока вершины добаляются и мы не дошли до финиша
                while (added /*&& DMatr.Get(x_to, y_to, 0) == -1*/)
                {
                    added = false;// Пока что ничего не добавили
                    min = mink;
                    for (int i = 0; i < x; i++)// Пробегаем по всей карте
                    {
                        for (int j = 0; j < y; j++)
                        {
                            // Если (i, j) была добавлена на предыдущем шаге и имеет минимальный вес
                            // Пробегаем по всем четырем сторонам
                            if (DMatr.Get(i, j, 0) == min)
                            {
                                int _i, _j, k;
                                _i = i + 1; _j = j;
                                // Если не вышли за пределы карты -  обрабатываем
                                if (_i >= 0 && _j >= 0 && _i < x && _j < y)
                                {
                                    DMatr.Set(_i, _j, 3, 2);
                                    k = Ves(i, j, _i, _j, mode);
                                    // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем
                                    if (((DMatr.Get(_i, _j, 0) == -1) || (DMatr.Get(_i, _j, 0) > k)) && ((DMatr.Get(_i, _j, 0) != -2) && (DMatr.Get(_i, _j, 0) != -3)))
                                    {
                                        
                                        // Добавляем
                                        if (mink == min)
                                        {
                                            mink = k;
                                        }
                                        else
                                        {
                                            if (k < mink)
                                                mink = k;
                                        }
                                        DMatr.Set(_i, _j, 1, i);
                                        DMatr.Set(_i, _j, 2, j);
                                        DMatr.Set(_i, _j, 0, k);
                                        added = true; // Добавили
                                    }
                                }
                                _i = i - 1; _j = j;
                                // Если не вышли за пределы карты -  обрабатываем
                                if (_i >= 0 && _j >= 0 && _i < x && _j < y)
                                {
                                    DMatr.Set(_i, _j, 3, 4);
                                    k = Ves(i, j, _i, _j, mode);
                                    // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем
                                    if (((DMatr.Get(_i, _j, 0) == -1) || (DMatr.Get(_i, _j, 0) > k)) && ((DMatr.Get(_i, _j, 0) != -2) && (DMatr.Get(_i, _j, 0) != -3)))
                                    {
                                        
                                       // Добавляем
                                        if (mink == min)
                                        {
                                            mink = k;
                                        }
                                        else
                                        {
                                            if (k < mink)
                                                mink = k;
                                        }
                                        DMatr.Set(_i, _j, 1, i);
                                        DMatr.Set(_i, _j, 2, j);
                                        DMatr.Set(_i, _j, 0, k);
                                        added = true; // Добавили
                                    }
                                }
                                _i = i; _j = j + 1;
                                // Если не вышли за пределы карты -  обрабатываем
                                if (_i >= 0 && _j >= 0 && _i < x && _j < y)
                                {
                                    DMatr.Set(_i, _j, 3, 1);
                                    k = Ves(i, j, _i, _j, mode);
                                    // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем
                                    if (((DMatr.Get(_i, _j, 0) == -1) || (DMatr.Get(_i, _j, 0) > k)) && ((DMatr.Get(_i, _j, 0) != -2) && (DMatr.Get(_i, _j, 0) != -3)))
                                    {
                                        
                                        // Добавляем
                                        if (mink == min)
                                        {
                                            mink = k;
                                        }
                                        else
                                        {
                                            if (k < mink)
                                                mink = k;
                                        }
                                        DMatr.Set(_i, _j, 1, i);
                                        DMatr.Set(_i, _j, 2, j);
                                        DMatr.Set(_i, _j, 0, k);
                                        added = true; // Добавили
                                    }
                                }
                                _i = i; _j = j - 1;
                                // Если не вышли за пределы карты -  обрабатываем
                                if (_i >= 0 && _j >= 0 && _i < x && _j < y)
                                {
                                    DMatr.Set(_i, _j, 3, 3);
                                    k = Ves(i, j, _i, _j, mode);
                                    // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем
                                    if (((DMatr.Get(_i, _j, 0) == -1) || (DMatr.Get(_i, _j, 0) > k)) && ((DMatr.Get(_i, _j, 0) != -2) && (DMatr.Get(_i, _j, 0) != -3)))
                                    {
                                        
                                         // Добавляем
                                        if (mink == min)
                                        {
                                            mink = k;
                                        }
                                        else
                                        {
                                            if (k < mink)
                                                mink = k;
                                        }
                                        DMatr.Set(_i, _j, 1, i);
                                        DMatr.Set(_i, _j, 2, j);
                                        DMatr.Set(_i, _j, 0, k);
                                        added = true; // Добавили
                                    }
                                }
                            }
                        }
                    }
                }
                if (DMatr.Get(x_to, y_to, 0) == -1)
                {
                    result = false; // то пути не существует
                    MessageBox.Show("Некотoрый путь не построен");
                }
                if (result)
                {
                    int _i = x_to, _j = y_to, li, lj, ki=x_to, kj=y_to, minto=Int32.MaxValue;
                    if (mode==2)
                    {
                        while (DMatr.Get(_i, _j, 0) != 0)
                        {
                            DMatr.Set(_i, _j, 0, -3);
                            li = _i; lj = _j + 1;
                            if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                            {
                                if ((DMatr.Get(li, lj, 0) <= minto) && (DMatr.Get(li, lj, 0) >= 0))
                                {
                                    if (DMatr.Get(li, lj, 0) < minto)
                                    {
                                        minto = DMatr.Get(li, lj, 0);
                                        ki = li;
                                        kj = lj;
                                    }
                                    else
                                    {
                                        if (DMatr.Get(_i, _j, 3) == DMatr.Get(li, lj, 3))
                                        {
                                            minto = DMatr.Get(li, lj, 0);
                                            ki = li;
                                            kj = lj;
                                        }
                                    }
                                }
                            }
                            li = _i + 1; lj = _j;
                            if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                            {
                                if ((DMatr.Get(li, lj, 0) <= minto) && (DMatr.Get(li, lj, 0) >= 0))
                                {
                                    if (DMatr.Get(li, lj, 0) < minto)
                                    {
                                        minto = DMatr.Get(li, lj, 0);
                                        ki = li;
                                        kj = lj;
                                    }
                                    else
                                    {
                                        if (DMatr.Get(_i, _j, 3) == DMatr.Get(li, lj, 3))
                                        {
                                            minto = DMatr.Get(li, lj, 0);
                                            ki = li;
                                            kj = lj;
                                        }
                                    }
                                }
                            }
                            li = _i; lj = _j - 1;
                            if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                            {
                                if ((DMatr.Get(li, lj, 0) <= minto) && (DMatr.Get(li, lj, 0) >= 0))
                                {
                                    if (DMatr.Get(li, lj, 0) < minto)
                                    {
                                        minto = DMatr.Get(li, lj, 0);
                                        ki = li;
                                        kj = lj;
                                    }
                                    else
                                    {
                                        if (DMatr.Get(_i, _j, 3) == DMatr.Get(li, lj, 3))
                                        {
                                            minto = DMatr.Get(li, lj, 0);
                                            ki = li;
                                            kj = lj;
                                        }
                                    }
                                }
                            }
                            li = _i - 1; lj = _j;
                            if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                            {
                                if ((DMatr.Get(li, lj, 0) <= minto) && (DMatr.Get(li, lj, 0) >= 0))
                                {
                                    if (DMatr.Get(li, lj, 0) < minto)
                                    {
                                        minto = DMatr.Get(li, lj, 0);
                                        ki = li;
                                        kj = lj;
                                    }
                                    else
                                    {
                                        if (DMatr.Get(_i, _j, 3) == DMatr.Get(li, lj, 3))
                                        {
                                            minto = DMatr.Get(li, lj, 0);
                                            ki = li;
                                            kj = lj;
                                        }
                                    }
                                }
                            }
                            _i = ki; _j = kj;
                        }
                        DMatr.Set(_i, _j, 0, -3);
                    }
                    else
                    {
                        while (DMatr.Get(_i, _j, 0) != 0) 
                        { 
                        DMatr.Set(_i, _j, 0, -3);
                        li = _i; lj = _j + 1;
                        if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                        {
                            if ((DMatr.Get(li, lj, 0) < minto) && (DMatr.Get(li, lj, 0) >= 0))
                            {
                                minto = DMatr.Get(li, lj, 0);
                                ki = li;
                                kj = lj;
                            }
                        }
                        li = _i + 1; lj = _j;
                        if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                        {
                            if ((DMatr.Get(li, lj, 0) < minto) && (DMatr.Get(li, lj, 0) >= 0))
                            {
                                minto = DMatr.Get(li, lj, 0);
                                ki = li;
                                kj = lj;
                            }
                        }
                        li = _i; lj = _j - 1;
                        if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                        {
                            if ((DMatr.Get(li, lj, 0) < minto) && (DMatr.Get(li, lj, 0) >= 0))
                            {
                                minto = DMatr.Get(li, lj, 0);
                                ki = li;
                                kj = lj;
                            }
                        }
                        li = _i - 1; lj = _j;
                        if ((li >= 0) && (lj >= 0) && (li < x) && (lj < y))
                        {
                            if ((DMatr.Get(li, lj, 0) < minto) && (DMatr.Get(li, lj, 0) >= 0))
                            {
                                minto = DMatr.Get(li, lj, 0);
                                ki = li;
                                kj = lj;
                            }
                        }
                        _i = ki; _j = kj;
                    }
                        DMatr.Set(_i, _j, 0, -3);
                    }
                }
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if ((DMatr.Get(i, j, 0) != -2) && (DMatr.Get(i, j, 0) != -3))
                        {
                            DMatr.Set(i, j, 0, -1);
                        }
                        DMatr.Set(i, j, 1, 0);
                        DMatr.Set(i, j, 2, 0);
                        DMatr.Set(i, j, 3, 0);
                    }
                }
            }
            try
            {
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if (DMatr.Get(i, j, 0) == -2)
                        {
                            image1.SetPixel(i, j, Color.Black);
                        }
                        if (DMatr.Get(i, j, 0) == -3)
                        {
                            image1.SetPixel(i, j, Color.LawnGreen);
                        }

                    }
                }
                image1.RotateFlip(RotateFlipType.Rotate180FlipX);
                SaveFileDialog saveFileDialog1 = new SaveFileDialog
                {
                    FileName = "",
                    DefaultExt = ".bmp",
                    Filter = "Image(.bmp)|*.bmp"
                };
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string filename = saveFileDialog1.FileName;
                    FileStream fstream = new FileStream(filename, FileMode.Create);
                    image1.Save(fstream, System.Drawing.Imaging.ImageFormat.Bmp);
                    fstream.Close();
                }
                image1.Dispose();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Error");
            }
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (DMatr.Get(i, j, 0) !=-2)
                    {
                        DMatr.Set(i, j, 0, -1);
                    }
                    DMatr.Set(i, j, 1, 0);
                    DMatr.Set(i, j, 2, 0);
                    DMatr.Set(i, j, 3, 0);
                }
            }
        }
        
        private int Ves(int i, int j, int _i, int _j, int modd)
        {
            int res = 0;
            int kol = 0;
            switch (modd)
            {
                case 0: res = res + DMatr.Get(i, j, 0) + 1;
                    break;
                case 1:
                    res = res + DMatr.Get(i, j, 0) + 1;
                    if (_i + 1 < DMatr.Getx())
                    {
                        if ((DMatr.Get(_i + 1, _j, 0) == -3) || (DMatr.Get(_i + 1, _j, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if (_j - 1 >= 0)
                    {
                        if ((DMatr.Get(_i, _j - 1, 0) == -3) || (DMatr.Get(_i, _j - 1, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if (_j + 1 < DMatr.Gety())
                    {
                        if ((DMatr.Get(_i, _j + 1, 0) == -3) || (DMatr.Get(_i, _j + 1, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if (_i - 1 >= 0)
                    {
                        if ((DMatr.Get(_i - 1, _j, 0) == -3) || (DMatr.Get(_i - 1, _j, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if ((_i + 1 <DMatr.Getx())&&(_j+1<DMatr.Gety()))
                    {
                        if ((DMatr.Get(_i + 1, _j+1, 0) == -3) || (DMatr.Get(_i + 1, _j+1, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if ((_i +1<DMatr.Getx())&&(_j-1>=0))
                    {
                        if ((DMatr.Get(_i + 1, _j-1, 0) == -3) || (DMatr.Get(_i + 1, _j-1, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if ((_i - 1 >= 0)&&(_j-1>=0))
                    {
                        if ((DMatr.Get(_i - 1, _j-1, 0) == -3) || (DMatr.Get(_i - 1, _j-1, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    if ((_i - 1 >= 0)&&(_j+1<DMatr.Gety()))
                    {
                        if ((DMatr.Get(_i - 1, _j+1, 0) == -3) || (DMatr.Get(_i - 1, _j+1, 0) == -2))
                        {
                            kol += 1;
                        }
                    }
                    res = res + kol;
                    break;
                case 2:
                    res = res + DMatr.Get(i, j, 0) + 1;
                    if (DMatr.Get(i, j, 3) != DMatr.Get(_i, _j, 3))
                        res+=1;
                    break;
            }
            return res;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Покорнейше надеюсь, что в файле данные лежат должным образом, без всякой дичи, отделенные любезно одними лишь пробелами, да переходами на новую строку.\nВообще" +
                 " же ожидаю видеть всё в таком формате:\nНачало-Х НачалоУ Конец-Х Конец-У", "Обрати внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            OpenFileDialog openFileDialog1 = new OpenFileDialog {
                Filter = "Текстовички (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex = 1
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                {
                    int nx, ny, kx, ky;
                    string[] sp;
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        sp = line.Split(' ');
                        nx = Convert.ToInt32(sp[0]) - 1;
                        ny = Convert.ToInt32(sp[1])-1;
                        kx = Convert.ToInt32(sp[2])-1;
                        ky = Convert.ToInt32(sp[3])-1;
                        LTrack.Add(new Traces(nx, ny, kx, ky));
                    }
                    pointOK.Visible = true;
                }
            }
        }

        private void ClosePic_MouseEnter(object sender, EventArgs e)
        {
            ClosePic.BackColor = Color.FromArgb(222, 126, 126);
        }

        private void MaxPic_MouseEnter(object sender, EventArgs e)
        {
            MaxPic.BackColor = Color.FromArgb(199, 231, 237);
        }

        private void MinPic_MouseEnter(object sender, EventArgs e)
        {
            MinPic.BackColor = Color.FromArgb(199, 231, 237);
        }

        private void ClosePic_Click(object sender, EventArgs e)
        {
            outp = true;
            sound1.Stop();
            Application.Exit();
        }

        private void MaxPic_Click(object sender, EventArgs e)
        {
            if (!spl)
            {
                this.WindowState = FormWindowState.Maximized;
                spl = true;
                return;
            }
            if (spl)
            {
                this.WindowState = FormWindowState.Normal;
                spl = false;
                return;
            }
        }

        private void MinPic_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ClosePic_MouseLeave(object sender, EventArgs e)
        {
            ClosePic.BackColor = SystemColors.ControlLightLight; ;
        }

        private void MaxPic_MouseLeave(object sender, EventArgs e)
        {
            MaxPic.BackColor = SystemColors.ControlLightLight;
        }

        private void MinPic_MouseLeave(object sender, EventArgs e)
        {
            MinPic.BackColor = SystemColors.ControlLightLight;
        }

        private void TipZag_MouseDown(object sender, MouseEventArgs e)
        {
            ((Control)sender).Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }

        private void Label3_MouseDown(object sender, MouseEventArgs e)
        {
            ((Control)sender).Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }

        private void ResBut_Click(object sender, EventArgs e)
        {
            LTrack.Clear();
            DMatr = null;
            mapOK.Visible = false;
            pointOK.Visible = false;
        }

        private void Op1_CheckedChanged(object sender, EventArgs e)
        {
            if (op1.Checked)
            {
                mode = 0;
            }
            if (op2.Checked)
            {
                mode = 1;
            }
            if (op3.Checked)
            {
                mode = 2;
            }
        }

        private void Op2_CheckedChanged(object sender, EventArgs e)
        {
            if (op1.Checked)
            {
                mode = 0;
            }
            if (op2.Checked)
            {
                mode = 1;
            }
            if (op3.Checked)
            {
                mode = 2;
            }
        }

        private void Op3_CheckedChanged(object sender, EventArgs e)
        {
            if (op1.Checked)
            {
                mode = 0;
            }
            if (op2.Checked)
            {
                mode = 1;
            }
            if (op3.Checked)
            {
                mode = 2;
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Hide();
            Mmove.LearnToMove(shadow);
        }

        private void Dich_Click(object sender, EventArgs e)
        {
            this.shadow.Top = 295;
            this.shadow.Left = 12;
            Random rr = new Random();
            screamer forma = new screamer();
            int zd = rr.Next(10,45);
            DateTime end = DateTime.Now + TimeSpan.FromSeconds(zd);
            sound1.Play();
            try
            {
                while (end > DateTime.Now && !outp)
                {
                    Application.DoEvents();
                }
                sound1.Stop();
                forma.ShowInTaskbar = false;
                forma.ShowDialog(this);
            }
            catch
            {
                return;
            }
        }

        private void Dlico_MouseDown(object sender, MouseEventArgs e)
        {
            ((Control)sender).Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }
    }
    public abstract class Mmove
    {

        static bool isPress = false;
        static Point startPst;
        /// <summary>
        /// Функция выполняется при нажатии на перемещаемый контрол
        /// </summary>
        /// <param name="sender">контролл</param>
        /// <param name="e">событие мышки</param>
        private static void MDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;//проверка что нажата левая кнопка
            isPress = true;
            startPst = e.Location;
        }
        /// <summary>
        /// Функция выполняется при отжатии перемещаемого контрола
        /// </summary>
        /// <param name="sender">контролл</param>
        /// <param name="e">событие мышки</param>
        private static void MUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;//проверка что нажата левая кнопка
            isPress = false;
        }
        /// <summary>
        /// Функция выполняется при перемещении контрола
        /// </summary>
        /// <param name="sender">контролл</param>
        /// <param name="e">событие мышки</param>
        private static void MMove(object sender, MouseEventArgs e)
        {
            if (isPress)
            {
                Control control = (Control)sender;
                control.Top += e.Y - startPst.Y;
                control.Left += e.X - startPst.X;
            }
        }
        /// <summary>
        /// обучает контролы передвигаться
        /// </summary>
        /// <param name="sender">контролл(это может быть кнопка, лейбл, календарик и.т.д)</param>
        public static void LearnToMove(object sender)
        {
            Control control = (Control)sender;
            control.MouseDown += new MouseEventHandler(MDown);
            control.MouseUp += new MouseEventHandler(MUp);
            control.MouseMove += new MouseEventHandler(MMove);
        }
    }
    public class Traces
    {
        public int x, y, x_to, y_to;
        public Traces(int q1, int q2, int q3, int q4)
        {
            x = q1;
            y = q2;
            x_to = q3;
            y_to = q4;
        }
    }

    public class MapData
    {
        int[, ,] mas;
        int nx, ny;
        public MapData(int x, int y)
        {
            nx = x;
            ny = y;
            mas = new int[x, y, 4]; //0-вес, 1-предХ, 2-предУ, 3-путевая (1-вверх,2-вправо,3-вниз,4-влево)
        }
        public void Set(int x, int y, int z, int data)
        {
            mas[x, y, z] = data;
        }
        public int Get(int x, int y, int z)
        {
            return mas[x, y, z];
        }
        public int Getx()
        {
            return nx;
        }
        public int Gety()
        {
            return ny;
        }
    }
}
